package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func main() {
	// Call the helloWorld function
	helloWorld()

	// Switch to the merge_branch
	switchToMergeBranch()

	// Merge changes into the specified branches
	branches := getUserInputBranches()
	for _, branch := range branches {
		mergeChanges(branch)
	}

	// Set upstream branches for infr, test, and prod
	setUpstreamBranches(branches)
}

func helloWorld() {
	fmt.Println("Hello World")
}

func switchToMergeBranch() {
	cmd := exec.Command("git", "checkout", "-b", "merge_branch")
	err := cmd.Run()
	if err != nil {
		fmt.Println("Failed to switch to merge_branch")
		os.Exit(1)
	} else {
		fmt.Println("Switched to merge_branch")
	}
}

func mergeChanges(branch string) {
	cmd := exec.Command("git", "merge", "--no-ff", branch)
	err := cmd.Run()
	if err != nil {
		fmt.Printf("Failed to merge changes into %s branch\n", branch)
		os.Exit(1)
	} else {
		fmt.Printf("Merged changes into %s branch\n", branch)
	}
}

func setUpstreamBranch(branch string) {
	cmd := exec.Command("git", "branch", "-u", fmt.Sprintf("origin/%s", branch), branch)
	err := cmd.Run()
	if err != nil {
		fmt.Printf("Failed to set upstream branch for %s: %s\n", branch, err.Error())
	} else {
		fmt.Printf("Set upstream branch for %s\n", branch)
	}
}

func setUpstreamBranches(branches []string) {
	for _, branch := range branches {
		setUpstreamBranch(branch)
	}
}

func getUserInputBranches() []string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter the branch names to merge (separated by commas):")
	input, _ := reader.ReadString('\n')
	input = strings.TrimSpace(input)
	branches := strings.Split(input, ",")
	return branches
}
